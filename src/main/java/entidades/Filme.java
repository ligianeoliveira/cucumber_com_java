package entidades;

public class Filme {

    private int estoque;
    private int aluguel;

    public void setEstoque(int arg0) {
        this.estoque = arg0;
    }

    public void setAluguel(int arg0) {
        this.aluguel = arg0;
    }

    public int getAluguel() {
        return aluguel;
    }

    public int getEstoque() {
        return estoque;
    }
}
