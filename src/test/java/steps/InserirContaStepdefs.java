package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.io.File;
import java.io.IOException;

public class InserirContaStepdefs {

    private WebDriver driver;

    /*
@Dado("^que estou acessando a aplicação$")
public void queEstouAcessandoAAplicação() {
    driver = new ChromeDriver();
    driver.get("https://seubarriga.wcaquino.me");
}


@Quando("^informo o usuário \"([^\"]*)\"$")
public void informoOUsuário(String arg0) {
    driver.findElement(By.id("email")).sendKeys(arg0);
}

@E("^a senha \"([^\"]*)\"$")
public void aSenha(String arg0) {
    driver.findElement(By.name("senha")).sendKeys(arg0);
}

@E("^seleciono entrar$")
public void selecionoEntrar() {
    driver.findElement(By.tagName("button")).click();
}

@Então("^visualizo a página inicial$")
public void visualizoAPáginaInicial() {
    String texto = driver.findElement(By.xpath("//div[@class='alert alert-success']")).getText();
    Assert.assertEquals("Bem vindo, Ligiane Mara Oliveira!", texto);
}

@Quando("^seleciono Contas$")
public void selecionoContas() {
    driver.findElement(By.linkText("Contas")).click();
}

@E("^seleciono Adicionar$")
public void selecionoAdicionar() {
    driver.findElement(By.linkText("Adicionar")).click();
}

@E("^informo a conta \"([^\"]*)\"$")
public void informoAConta(String arg0){
    driver.findElement(By.id("nome")).sendKeys(arg0);
}

@E("^seleciono Salvar$")
public void selecionoSalvar() {
    driver.findElement(By.tagName("button")).click();
}
*/
    @Dado("^que desejo adicionar uma conta$")
    public void queDesejoAdicionarUmaConta() {
        driver = new ChromeDriver();
        driver.get("https://seubarriga.wcaquino.me");
        driver.findElement(By.id("email")).sendKeys("ligianeoliveira90@gmail.com");
        driver.findElement(By.name("senha")).sendKeys("971713");
        driver.findElement(By.tagName("button")).click();
        driver.findElement(By.linkText("Contas")).click();
        driver.findElement(By.linkText("Adicionar")).click();
    }

    @Quando("^adiciono a conta \"([^\"]*)\"$")
    public void adicionoAConta(String arg0){
        driver.findElement(By.id("nome")).sendKeys(arg0);
        driver.findElement(By.tagName("button")).click();
    }

    @Então("^recebo a mensagem \"([^\"]*)\"$")
    public void receboAMensagem(String arg0) {
        String texto = driver.findElement(By.xpath("//div[starts-with(@class, 'alert alert-')]")).getText();
        Assert.assertEquals(arg0, texto);
    }

    @After(order = 1, value = "@funcionais")
    public void screenshot(Scenario cenario){
        File file =  ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(file, new File ("target/screenshots/"+cenario.getId()+".jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @After(order = 0, value = "@funcionais")
    public void fecharBrowser(){
        driver.quit();
    }

}
