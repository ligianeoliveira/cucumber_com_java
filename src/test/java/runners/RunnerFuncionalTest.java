package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features/inserir_conta.feature",
        glue = "steps",
        tags = {"@funcionais"},
        plugin = {"pretty", "html:target/report-html", "json:target/report.json"},
        //serve para verificar se o mapeamento dos cenários está correto
        dryRun = false,
        //serve para ignorar novos passos que foram adicionados e não foram mapeados
        strict = false
)
public class RunnerFuncionalTest {

    //metodo para resetar a massa de dados que será executado antes dos metodos do Cucumber.clas
    @BeforeClass
    public static void reset(){
        WebDriver driver = new ChromeDriver();
        driver.get("https://seubarriga.wcaquino.me");
        driver.findElement(By.id("email")).sendKeys("ligianeoliveira90@gmail.com");
        driver.findElement(By.name("senha")).sendKeys("971713");
        driver.findElement(By.tagName("button")).click();
        driver.findElement(By.linkText("reset")).click();
        driver.quit();
    }
}
