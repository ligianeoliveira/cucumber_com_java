package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features/",
        glue = "steps",
        tags = {"@unitários"},
        plugin = {"pretty", "html:target/report-html", "json:target/report.json"},
        //serve para verificar se o mapeamento dos cenários está correto
        dryRun = false,
        //serve para ignorar novos passos que foram adicionados e não foram mapeados
        strict = false
)
public class RunnerTest {
}
